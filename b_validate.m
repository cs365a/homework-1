
1;
error1 = [];
error2 = [];
data = csvread ("training_data/h1_train_small.csv");
testData = csvread ("validation_data/h1_validate.csv");
testData(1,:)=[];
data(1,:)=[];
x = data(:, 1);
y = data(:, 2);
x1 = testData(:, 1);
y1 = testData(:, 2);
xx = [0 1 2 3 5 9];
for i = xx
	poly = polyfit (x, y, i);
	val = polyval(poly, x);
	error1(end+1) = sqrt(sum((val-y).^2)/numel(x));
	val = polyval(poly,x1);
	error2(end+1) = sqrt(sum((val-y1).^2)/numel(x1));
endfor
f1 = figure();
p2 = plot(xx, error2);
ylim auto;
set(p2, 'Color', 'blue', 'LineWidth', 1);
xlabel({'Degree of polynomial'});
ylabel({'Erms'});
title({'Graph of root-mean-square error evaluated on validation set using training set'});
legend('Validation Data');
saveas(f1, 'plots/small_validate.png');