
1;
error1 = [];
error2 = [];
data = csvread ("training_data/h1_train_big.csv");
testData = csvread ("test_data/h1_test.csv");
testData(1,:)=[];
data(1,:)=[];
x = data(:, 1);
y = data(:, 2);
x1 = testData(:, 1);
y1 = testData(:, 2);
polyList = [];
xx = [0 1 2 3 5 9];
ll = linspace(1, 10, 600);
for i = xx
	poly = polyfit (x, y, i);
	val = polyval(poly, x);
	error1(end+1) = sqrt(sum((val-y).^2)/numel(x));
	f = figure();
	plot(x, y, "+", ll, polyval(poly,ll));
	ylim ([0 600]);
	xlabel({'Time'});
	ylabel({'Height'});
	title({strcat('Polynomial of degree = ', int2str(i))});
	legend('Training Data', 'Polynomial');
	saveas(f, strcat('plots/big-', int2str(i), '.png'));
	hold off;
	val = polyval(poly,x1);
	error2(end+1) = sqrt(sum((val-y1).^2)/numel(x1));
	polyList(end+1, :) = [fliplr(poly) zeros(1, 9-i)];
endfor
f1 = figure();
p1 = plot(xx, error1);
hold on;
p2 = plot(xx, error2);
#ylim auto;
ylim ([0 100]);
set(p1, 'Color', 'red', 'LineWidth', 1);
set(p2, 'Color', 'blue', 'LineWidth', 1);
xlabel({'Degree of polynomial'});
ylabel({'Erms'});
title({'Graph of root-mean-square error evaluated on test set and training set'});
legend('Training Data', 'Test Data');
saveas(f1, 'plots/big_train_VS_test.png');
polyList = polyList';
csvwrite('output/coeff_big.csv', polyList);