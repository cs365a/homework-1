
1;
error1 = [];
error2 = [];
data = csvread ("training_data/h1_train_small.csv");
data(1,:)=[];
x = data(:, 1);
y = data(:, 2);
xx = [0 1 2 3 5 9];
for i = xx
	poly = polyfit (x, y, i);
	val = polyval(poly, x);
	error1(end+1) = sqrt(sum((val-y).^2)/numel(x));
endfor
f1 = figure();
p1 = plot(xx, error1);
xlabel({'Degree of polynomial'});
ylabel({'Erms'});
title({'Graph of root-mean-square error evaluated on training set'});
legend('Training Data');
saveas(f1, 'plots/small_train.png');