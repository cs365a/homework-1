Homework 1(Assignment on Overfitting and Complex Models)

========================================================

Project Structure
	Directory 'output' contains all the generated '.csv' files.
	Directory 'plots' contains all the generated '.png' image files for different plots in the assignment.
	Rest are directories for provided data.

========================================================

All files are GNU Octave script files.

Files:
	a.m
		Plots all polynomials of different degree to show the fitting for small data set. Also plots Erms for training set and test set on the same plot. Finally generates coefficient matrix for the different polynomials in file 'coeff_small.csv'.
	a_test.m
		Plots Erms for test set only for small data set. Also generates error for this set in file 'error_small_test.csv'.
	a_training.m
		Plots Erms for training set only for small data set.
	b.m
		Plots Erms for validation set and training set together for small data set. Also generates error for training set and validation set in file 'error_small.csv'.
	b_validate.m
		Plots Erms for validation set only for small data set.
	c.m
		Plots all polynomials of different degree to show the fitting for big data set. Also plots Erms for training set and test set on the same plot. Finally generates coefficient matrix for the different polynomials in file 'coeff_big.csv'.
	c_test.m
		Plots Erms for test set only for big data set. Also generates error for this set in file 'error_big_test.csv'.
	c_training.m
		Plots Erms for training set only for big data set.
	c_validate.m
		Plots Erms for validation set only for big data set.
	c_validate_2.m
		Plots Erms for validation set and training set together for big data set. Also generates error for training set and validation set in file 'error_big.csv'.

========================================================